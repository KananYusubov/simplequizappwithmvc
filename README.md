**Simple Quiz App using MVC pattern (Easy way)**
------------------------------------------------
> This app includes two activities. First is to register user or start the game.

>  Second is to play game. Here there are some questions that are fetched using different methods.

> At the end of game, user is able to see him/her result. 

> Username and previous score of user are saved in SharedPreferences. That is why, we can store different values permanently.

> There is a bug in game. When we play game and at the same time rotate screen to landscape mode, question will be change or it will be endless. 

> If you have solution, please send PR (pull request).