package com.example.kananyusub.designpaternlearn.controller;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kananyusub.designpaternlearn.R;
import com.example.kananyusub.designpaternlearn.model.User;


public class MainActivity extends AppCompatActivity {
    private static final int GAME_ACTIVITY_REQUEST_CODE = 16;
    private User mUser;
    private EditText mUserNameInput;
    private Button mPlayButton;
    private TextView mMessageTextView;
    private SharedPreferences mPreferences;
    public static final String PREF_KEY_NAME = "PREFERENCE_KEY_NAME";
    public static final String PREF_KEY_SCORE = "PREFERENCE_KEY_SCORE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mUserNameInput = findViewById(R.id.edittext_main_username);
        mPlayButton = findViewById(R.id.button_main_play);
        mMessageTextView = findViewById(R.id.textview_main_greeting_txt);
        mUser = new User();
        mPreferences = getPreferences(MODE_PRIVATE);

        displayInfo();

        mUserNameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mPlayButton.setEnabled(s.length() > 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUser.setFirstName(mUserNameInput.getText().toString());
                mPreferences.edit().putString(PREF_KEY_NAME, mUser.getFirstName()).apply();
                startActivityForResult(new Intent(MainActivity.this, GameActivity.class), GAME_ACTIVITY_REQUEST_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GAME_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                int score = data.getIntExtra(GameActivity.BUNDLE_EXTRA_SCORE, 0);
                mPreferences.edit().putInt(PREF_KEY_SCORE, score).apply();
                displayInfo();
                Toast.makeText(this, "Your Score: " + score, Toast.LENGTH_SHORT).show();
            }else {
                displayInfo();
            }
        }
    }

    private void displayInfo() {
        if (mPreferences.contains(PREF_KEY_NAME)) {
            String name = mPreferences.getString(PREF_KEY_NAME, "");
            mUserNameInput.setVisibility(View.GONE);
            mPlayButton.setEnabled(true);
            if (mPreferences.contains(PREF_KEY_SCORE)) {
                int score = mPreferences.getInt(PREF_KEY_SCORE, 0);
                mMessageTextView.setText(name + ": " + score);
            } else {
                mMessageTextView.setText("Welcome back, Kanan!");
            }
        } else {
            mPlayButton.setEnabled(mUserNameInput.getText().toString().length() > 0);
            mMessageTextView.setText("Welcome to Quiz App");
        }
    }
}
