package com.example.kananyusub.designpaternlearn.controller;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kananyusub.designpaternlearn.R;
import com.example.kananyusub.designpaternlearn.model.Question;
import com.example.kananyusub.designpaternlearn.model.QuestionBank;

import java.lang.ref.WeakReference;
import java.util.Arrays;

public class GameActivity extends AppCompatActivity
        implements View.OnClickListener {
    public static final String BUNDLE_EXTRA_SCORE =
            GameActivity.class.getCanonicalName().concat("BUNDLE_EXTRA_SCORE");
    public static final String BUNDLE_STATE_SCORE =
            GameActivity.class.getCanonicalName().concat("BUNDLE_STATE_SCORE");
    public static final String BUNLDE_STATE_QUESTIONS =
            GameActivity.class.getCanonicalName().concat("BUNDLE_STATE_QUESTIONS");
    public static final String BUNDLE_STATE_QUESTION_BANK =
            GameActivity.class.getCanonicalName().concat("BUNDLE_STATE_QUESTION_BANK");
    private TextView mQuestionTextView;
    private Button mAnswerButton1, mAnswerButton2, mAnswerButton3, mAnswerButton4;
    private QuestionBank mQuestionBank;
    private Question mCurrentQuestion;
    private int mNumberOfQuestions;
    private int mScore;
    private boolean mEnableTouchEvents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        mQuestionBank = generateQuestions();

        if (savedInstanceState != null) {
            mScore = savedInstanceState.getInt(BUNDLE_STATE_SCORE);
            mNumberOfQuestions = savedInstanceState.getInt(BUNLDE_STATE_QUESTIONS);
            mQuestionBank = (QuestionBank) savedInstanceState.getSerializable(BUNDLE_STATE_QUESTION_BANK);
        } else {
            mScore = 0;
            mNumberOfQuestions = 3;
        }

        mEnableTouchEvents = true;


        mQuestionTextView = findViewById(R.id.textview_game_question_text);
        mAnswerButton1 = findViewById(R.id.button_game_answer1);
        mAnswerButton2 = findViewById(R.id.button_game_answer2);
        mAnswerButton3 = findViewById(R.id.button_game_answer3);
        mAnswerButton4 = findViewById(R.id.button_game_answer4);

        mAnswerButton1.setTag(0);
        mAnswerButton2.setTag(1);
        mAnswerButton3.setTag(2);
        mAnswerButton4.setTag(3);

        mCurrentQuestion = mQuestionBank.getQuestion();
        displayQuestion(mCurrentQuestion);
    }

    private void displayQuestion(final Question question) {
        mQuestionTextView.setText(question.getQuestion());
        mAnswerButton1.setText(question.getChoiceList().get(0));
        mAnswerButton2.setText(question.getChoiceList().get(1));
        mAnswerButton3.setText(question.getChoiceList().get(2));
        mAnswerButton4.setText(question.getChoiceList().get(3));

        mAnswerButton1.setOnClickListener(this);
        mAnswerButton2.setOnClickListener(this);
        mAnswerButton3.setOnClickListener(this);
        mAnswerButton4.setOnClickListener(this);
    }

    private QuestionBank generateQuestions() {
        Question question1 = new Question("Who created Android?",
                Arrays.asList("Andy Rubin",
                        "Steve Wozniak",
                        "Jake Wharton",
                        "Paul Smith"),
                0);

        Question question2 = new Question("When did the first person land on the moon?",
                Arrays.asList("1958",
                        "1962",
                        "1967",
                        "1969"),
                3);

        Question question3 = new Question("What is the house number of The Simpsons?",
                Arrays.asList("42",
                        "101",
                        "666",
                        "742"),
                3);

        return new QuestionBank(Arrays.asList(question1,
                question2,
                question3));
    }

    //region OnClickListener Implementation
    @Override
    public void onClick(View v) {
        int userAnswer = (int) v.getTag();
        if (userAnswer == mCurrentQuestion.getAnswerIndex()) {
            // when the answer is true
            Toast.makeText(this, "Congrats!", Toast.LENGTH_SHORT).show();
            mScore++;
        } else {
            // when the answer is false
            Toast.makeText(this, "Wrong!", Toast.LENGTH_SHORT).show();
        }

        mEnableTouchEvents = false;
        MyHandler mHandler = new MyHandler(this);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (--mNumberOfQuestions == 0) {
                    //end the game
                    endGame();
                } else {
                    // print next question
                    mCurrentQuestion = mQuestionBank.getQuestion();
                    displayQuestion(mCurrentQuestion);
                }
                mEnableTouchEvents = true;
            }
        }, 2000);
    }
    //endregion

    // region EndGameMethod
    private void endGame() {
        new AlertDialog.Builder(this)
                .setTitle("Well Done!")
                .setMessage("Your Score: " + mScore)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // before game activity is destroyed we send score of user back to the main activity
                        Intent intent = new Intent();
                        intent.putExtra(BUNDLE_EXTRA_SCORE, mScore);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                })
                .create()
                .show();
    }
    //endregion

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return mEnableTouchEvents && super.dispatchTouchEvent(ev);
    }

    private static class MyHandler extends Handler {
        private final WeakReference<GameActivity> mActivity;

        private MyHandler(GameActivity activity) {
            mActivity = new WeakReference<GameActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            GameActivity activity = mActivity.get();
            if (activity != null) {
                // give your message
                Toast.makeText(activity, "HANDLER", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(BUNDLE_STATE_SCORE, mScore);
        outState.putInt(BUNLDE_STATE_QUESTIONS, mNumberOfQuestions);
        outState.putSerializable(BUNDLE_STATE_QUESTION_BANK, mQuestionBank);
        super.onSaveInstanceState(outState);
    }
}
